from django.conf.urls import include, url
from django.contrib import admin

from accounts import views


urlpatterns = [
    url(r'^$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
]
