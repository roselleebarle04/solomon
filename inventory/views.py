from django.shortcuts import render
from django.http import HttpResponse

from .models import Item, Supplier, Category, Brand, ItemModel

def add_item(request):
	if request.method == 'POST':
		# Save the rest of the fields from the POST trigger
		store_code = request.POST.get('store_code')
		supplier_code = request.POST.get('supplier_code')
		unit_cost = request.POST.get('unit_cost')

		# Todo : Move to models (Fat Models, Stupid Views)
		new_item = Item(store_code=store_code, supplier_code=supplier_code, unit_cost=unit_cost)
		new_item.status = "ACTIVE"
		new_item.save()
	return HttpResponse({""})

def add_supplier(request):
	if request.method == 'POST':
		name = request.POST.get('name')
		address = request.POST.get('address')
		phone = request.POST.get('phone')

		new_supplier = Supplier(name=name, address=address, phone=phone)
		new_supplier.save()
	return HttpResponse({""})

def add_category(request):
	if request.method == 'POST':
		description = request.POST.get('description')
		new_category = Category(description=description)
		new_category.save()
	return HttpResponse({""})

def add_brand(request):
	if request.method == 'POST':
		description = request.POST.get('description')
		new_brand = Brand(description=description)
	return HttpResponse({""})

def add_item_model(request):
	if request.method == 'POST':
		description = request.POST.get('description')
		new_item_model = ItemModel(description=description)
		new_item_model.save()
	return HttpResponse({""})	
















"""def dashboard(request):
	return render(request, 'dashboard/home.html', {})

def add_item(request):
	return render(request, 'dashboard/add_item.html', {})

def add_arrival(request):
	return render(request, 'dashboard/add_arrival.html', {})

def add_sales(request):
	return render(request, 'dashboard/add_sales.html', {})

def transfer_item(request):
	return render(request, 'dashboard/transfer_item.html', {})

def notifications(request):
	return render(request, 'dashboard/notifications.html', {})

def item_reports (request):
	item_list = Item.objects.all()
	item_list_length = len(item_list)
	inventory_balance = Item.get_inventory_balance()
	print inventory_balance

	return render(request, 'reports/item_reports.html', {
		'items': item_list,
		'length' : item_list_length,
		'inventory_value': inventory_balance,
	})

def arrival_reports (request):
	return render(request, 'reports/arrival_reports.html', {})

def sales_reports (request):
	sales_list = Sale.objects.all()
	sales_list_length = len(sales_list)
	return render(request, 'reports/sales_reports.html', {
		'date_period': 'October',
		'sales': sales_list, 
		'sales_length' : sales_list_length,
		'sales_total'  : Sale.grand_total()
	})

def transfer_reports (request):
	return render(request, 'reports/transfer_reports.html', {})"""