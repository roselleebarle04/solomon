# USING POSTGRESQL 
*** https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04 ***

### 1. Set up POSTGRES
#### The following apt commands will get you the packages you need:

``` sudo apt-get update
sudo apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib ```

#### Create a Database and Database User

``` 
sudo su - postgres
psql 

CREATE DATABASE myproject;
CREATE USER myprojectuser WITH PASSWORD 'password';

ALTER ROLE myprojectuser SET client_encoding TO 'utf8';
ALTER ROLE myprojectuser SET default_transaction_isolation TO 'read committed';
ALTER ROLE myprojectuser SET timezone TO 'UTC';
```

### 2. Install PSYCOPG2
``` pip install django psycopg2 ```

### 3. Update Settings.py 

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'myproject',
        'USER': 'myprojectuser',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '',
    }
}

### 4. Migrate the Database and Test
python manage.py makemigrations
python manage.py migrate

After creating the database structure, we can create an administrative account by typing:
python manage.py createsuperuser

# USING AND CUSTOMIZING DJANGO'S AUTH 
*** http://www.effectivedjango.com/tutorial/authzn.html ***