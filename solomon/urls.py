from django.conf.urls import include, url
from django.contrib import admin

from accounts import urls as accounts_urls
# from dashboard import urls as dashboard_urls
# from dashboard import views as dashboard_views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^account/', include(accounts_urls, namespace='accounts')),
    
    # url(r'^$', dashboard_views.dashboard, name='home'),
    # url(r'^add/item/', dashboard_views.add_item, name='add_item'),
    # url(r'^add/arrival/', dashboard_views.add_arrival, name='add_arrival'),
    # url(r'^add/sales/', dashboard_views.add_sales, name='add_sales'),
    # url(r'^transfer/', dashboard_views.transfer_item, name='transfer_item'),

    # url(r'^reports/items/', dashboard_views.item_reports, name='item_reports'),
    # url(r'^reports/arrivals/', dashboard_views.arrival_reports, name='arrival_reports'),
    # url(r'^reports/sales/', dashboard_views.sales_reports, name='sales_reports'),
    # url(r'^reports/transfers/', dashboard_views.transfer_reports, name='transfer_reports'),

    # url(r'^notifications/', dashboard_views.notifications, name='notifications'),
]
